import socket
import threading
import time

print_lock = threading.Lock()

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 8001))
s.listen(1)

def recv(conn):
    with conn:
        while True:
            data = conn.recv(1024)
            if data:
                with print_lock:
                    print(data)

def send(conn):
    with conn:
        while True:
            time.sleep(3)
            conn.send(b'ping\n')

def initialize_thread(target_func, conn):
    thread = threading.Thread(target=target_func, args=(conn,))
    thread.daemon = True
    thread.start()  

try:
    conn, addr = s.accept()

    print('Connected by', addr)

    initialize_thread(recv, conn)
    initialize_thread(send, conn)

    while True:
        time.sleep(5)

except KeyboardInterrupt:
    pass

finally:
    s.shutdown(socket.SHUT_RDWR)
    s.close()