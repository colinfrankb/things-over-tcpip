import socket

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind(('', 8001))

try:
    while True:
        print('Waiting to receive...')
        data = s.recv(1024)
        if data:
            print(data)

except KeyboardInterrupt:
    pass

finally:
    s.close()